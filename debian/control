Source: atropos
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               cython3,
               python3-all-dev,
               python3-setuptools,
               python3-setuptools-scm,
               python3-wheel,
               python3-loguru,
               python3-xphyle,
               python3-pytest <!nocheck>,
               python3-jinja2 <!nocheck>,
               python3-pysam <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/atropos
Vcs-Git: https://salsa.debian.org/med-team/atropos.git
Homepage: https://github.com/jdidion/atropos
Rules-Requires-Root: no

Package: atropos
Architecture: any
Depends: ${shlibs:Depends},
         ${python3:Depends},
         ${misc:Depends},
         python3-pysam
Recommends: python3-jinja2
Description: NGS read trimming tool that is specific, sensitive, and speedy
 Atropos is tool for specific, sensitive, and speedy trimming of NGS
 reads. It is a fork of the venerable Cutadapt read trimmer, with the
 primary improvements being:
 .
   1. Multi-threading support, including an extremely fast "parallel
      write" mode.
   2. Implementation of a new insert alignment-based trimming algorithm
      for paired-end reads that is substantially more sensitive and
      specific than the original Cutadapt adapter alignment-based
      algorithm. This algorithm can also correct mismatches between the
      overlapping portions of the reads.
   3. Options for trimming specific types of data (miRNA, bisulfite-seq).
   4. A new command ('detect') that will detect adapter sequences and
      other potential contaminants.
   5. A new command ('error') that will estimate the sequencing error
      rate, which helps to select the appropriate adapter- and quality-
      trimming parameter values.
   6. A new command ('qc') that generates read statistics similar to
      FastQC. The trim command can also compute read statistics both
      before and after trimming (using the '--stats' option).
   7. Improved summary reports, including support for serialization
      formats (JSON, YAML, pickle), support for user-defined templates
      (via the optional Jinja2 dependency), and integration with MultiQC.
   8. The ability to merge overlapping reads (this is experimental and
      the functionality is limited).
   9. The ability to write the summary report and log messages to
      separate files.
  10. The ability to read SAM/BAM files and read/write interleaved
      FASTQ files.
  11. Direct trimming of reads from an SRA accession.
  12. A progress bar, and other minor usability enhancements.
